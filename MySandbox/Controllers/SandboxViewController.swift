class SandboxViewController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var facebookButton: FBSDKLoginButton!
    @IBOutlet weak var googleButton: GIDSignInButton!
    @IBOutlet weak var googleSignInButton: UIButton!
    
    var facebookAuthorizer: FacebookAuthorizer?
    var googleAuthorizer: GoogleAuthorizer?
    var activityIndicatorPlugin = NVActivityIndicatorViewPlugin()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        googleButton.colorScheme = .dark
        
        facebookAuthorizer = FacebookAuthorizer(button: facebookButton, delegate: self)
        googleAuthorizer = GoogleAuthorizer(delegate: self, activityIndicator: activityIndicatorPlugin.activityIndicatorView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func signInTapped(_ sender: Any) {
        let userDraft = UserDraft()
        userDraft.email = emailTextField.text
        userDraft.password = passwordTextField.text
        
        let request = API.Session.open(userDraft).progressTitle(title: "Sign in...",
                                                                successTitle: "Success",
                                                                errorTitle: "Error")
        request.stubbingEnabled = true
        request.apiStub.buildModel(fromFileNamed: "openSession")
        request.apiStub.stubDelay = 0.5
        
        request.perform(withSuccess: { session in
            print("Session token (email): \(session.token)")
        }, failure: { error in
            print("Open session failed: \(String(describing: error.error?.localizedDescription))")
        })
    }
    
    @IBAction func googleSignInTapped(_ sender: Any) {
        activityIndicatorPlugin.activityIndicatorView.startAnimating()
        googleAuthorizer?.authorize()
    }
}

// MARK: FacebookAuthorizationDelegate
extension SandboxViewController: FacebookAuthorizationDelegate {
    func facebookAuthorizerSucceededWithToken(_ token: String) {
        print("Session token (facebook): \(token)")
    }
    
    func facebookAuthorizerFailedWithError(_ error: Error?) {
        if let error = error {
            AlertView.show(title: "ERROR".localized, message: error.localizedDescription)
        }
        facebookAuthorizer?.logOut()
    }
}

// MARK: GoogleAuthorizationDelegate
extension SandboxViewController: GoogleAuthorizationDelegate {
    func googleAuthorizerSucceededWithToken(_ token: String) {
        print("Session token (google): \(token)")
    }
    
    func googleAuthorizerFailedWithError(_ error: Error?) {
        if let error = error {
            AlertView.show(title: "ERROR".localized, message: error.localizedDescription)
        }
        googleAuthorizer?.signOut()
    }
}
