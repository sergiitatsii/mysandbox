enum AppAppearance {
    
    static func apply() {
        styleNavigationBar()
        styleBarButtonItem()
        styleSearchBarAppearance()
    }
    
    static func styleNavigationBar() {
        
    }
    
    static func styleBarButtonItem() {
        
    }
    
    static func styleSearchBarAppearance() {
        
    }
    
}
