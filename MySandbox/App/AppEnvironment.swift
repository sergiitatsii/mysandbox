class AppEnvironment {
    static let shared = AppEnvironment(debugService: PrintDebugOutputService())
    
    let configuration: String
    let serverURL: String
    var apiURL: String { get { return "https://\(serverURL)/api/" } }
    var apiVersion: String = "1.0.0"
    let appVersion: String
    
    let facebookPermissions: [String] = ["public_profile", "email", "user_friends"]
    var appMarketLink : URL? { return URL(string: "itms-apps://itunes.apple.com/app/id_of_app") }
    
    init(debugService: DebugOutputProtocol) {
        guard let configuration = Bundle.main.infoDictionary?["Configuration"] as? String,
            let path = Bundle.main.path(forResource: "Environments", ofType: "plist"),
            let environments = NSDictionary(contentsOfFile: path)
            else {
                fatalError("Failed to load Environments file")
        }
        
        self.configuration = configuration
        
        let currentEnvironment = environments.object(forKey: self.configuration) as? [String : String] ?? [:]
        serverURL = currentEnvironment["serverURL"] ?? ""
        let info = Bundle.main.infoDictionary
        appVersion = "\(info?["CFBundleShortVersionString"] as? String ?? "")(\(info?["CFBundleVersion"] as? String ?? ""))"
        
        debugService.debugOutput("Configuration: \(configuration)", sender: self)
        debugService.debugOutput("Server: \(serverURL)", sender: self)
    }
}
