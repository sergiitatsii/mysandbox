func delay(_ delay:Double, _ closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: closure)
}
