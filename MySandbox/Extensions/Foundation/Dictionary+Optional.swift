extension Dictionary where Key: ExpressibleByStringLiteral, Value: Any {
    mutating func addOptional(_ value: Value?, to key: Key) {
        if let value = value { self[key] = value }
    }
}
