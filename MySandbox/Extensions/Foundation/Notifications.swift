public extension Notification.Name {
    public static let SessionStarted = Notification.Name(rawValue: "SessionNotifications.SessionStarted")
    public static let SessionEnded = Notification.Name(rawValue: "SessionNotifications.SessionEnded")
    public static let UserUpdated = Notification.Name(rawValue: "SessionNotifications.UserUpdated")
}
