extension UIFont {
    enum MySandbox {
        static func corbel(_ size: CGFloat) -> UIFont {
            return UIFont(name: "Corbel", size: size) ?? UIFont.systemFont(ofSize: size)
        }
    }
}
