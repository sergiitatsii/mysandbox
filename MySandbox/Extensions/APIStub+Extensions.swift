public extension APIStub {
    public func buildError(fromFileNamed fileName: String, inBundle bundle: Bundle = Bundle.main) {
        if let filePath = bundle.path(forResource: fileName as String, ofType: nil)
        {
            errorData = try? Data(contentsOf: URL(fileURLWithPath: filePath))
        } else {
            print("Failed to build error from \(fileName) in \(bundle)")
        }
    }
}
