enum API {
    static let tron: TRON = TRON(baseURL: AppEnvironment.shared.apiURL,
                                 plugins: [NetworkLoggerPlugin(),
                                           NetworkActivityPlugin(application: UIApplication.shared),
                                           UnauthorizedHandlerPlugin()]).then { $0.headerBuilder = HeaderBuilder() }
}
