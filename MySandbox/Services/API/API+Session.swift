extension API {
    struct Session {
        static func open(_ userDraft: UserDraft) -> APIRequest<AppToken, DictionaryError> {
            return tron.swiftyJSON.request("session").then {
                $0.authorizationRequirement = .none
                $0.method = .post
                $0.parameters = userDraft.parametersForRequest()
            }
        }

        static func open(_ facebookToken: FacebookToken) -> APIRequest<AppToken, DictionaryError> {
            let request: APIRequest<AppToken, DictionaryError> = tron.swiftyJSON.request("facebook/session")
            request.authorizationRequirement = .none
            request.method = .post
            request.parameters["access_token"] = facebookToken.token
            return request
        }
        
        static func open(_ googleToken: GoogleToken) -> APIRequest<AppToken, DictionaryError> {
            let request: APIRequest<AppToken, DictionaryError> = tron.swiftyJSON.request("google/session")
            request.authorizationRequirement = .none
            request.method = .post
            request.parameters["access_token"] = googleToken.token
            return request
        }
    
        static func close() -> APIRequest<EmptyResponse, DictionaryError> {
            return tron.swiftyJSON.request("session").then {
                $0.authorizationRequirement = .required
                $0.method = .delete
            }
        }
    }
}
