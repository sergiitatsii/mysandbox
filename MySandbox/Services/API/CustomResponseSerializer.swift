import Alamofire

//struct CustomResponseSerializer<T, ErrorModel: JSONDecodable> : ErrorHandlingDataResponseSerializerProtocol
//{
//    typealias SerializedObject = T
//    typealias SerializedError = ErrorModel
//
//    let parser : (Data) throws -> T
//
//    init(dataParser: @escaping (Data) throws -> T ) {
//        self.parser = dataParser
//    }
//
//    var serializeResponse: (URLRequest?, HTTPURLResponse?, Data?, Error?) -> Result<T> {
//        return { request, response, data, error in
//            do {
//                let result = try self.parser(data.tryUnwrap())
//                return Result.success(result)
//            } catch {
//                return Result.failure(error)
//            }
//        }
//    }
//
//    var serializeError: (Result<T>?, URLRequest?, HTTPURLResponse?, Data?, Error?) -> APIError<ErrorModel>
//}
