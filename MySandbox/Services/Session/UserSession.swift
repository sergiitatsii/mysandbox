class UserSession {
    static let shared = UserSession()
    
    var tokenStorage: UserDefaultsTokenStorage = UserDefaultsTokenStorage(defaultsKey: Constants.UserDefaultsKeys.AuthToken, debugService: PrintDebugOutputService())
    
    var authToken : String? {
        get {
            return tokenStorage.token
        }
        set {
            tokenStorage.token = newValue
        }
    }
    
    var pushToken: String?
    
    func logout() {
        
    }
}
