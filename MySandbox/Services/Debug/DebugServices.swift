protocol DebugOutputProtocol {
    func debugOutput<T>(_ message: String, sender: T?)
    func debugOutput(_ message: String)
    
    func debugDeinit<T>(_ sender: T?)
    func debugDeinit<T>(_ message: String, sender: T?)
}

extension DebugOutputProtocol {
    fileprivate func senderType<T>(_ sender: T?) -> String {
        return sender == nil ? "" : "[\(String(describing: T.self))]"
    }
}

class PrintDebugOutputService : DebugOutputProtocol {
    func debugOutput<T>(_ message: String, sender: T?) {
        print("\(senderType(sender)) \(message)")
    }
    func debugOutput(_ message: String) {
        print(message)
    }
    
    func debugDeinit<T>(_ message: String, sender: T?) {
//        print("~\(senderType(sender)) \(message)")
    }
    func debugDeinit<T>(_ sender: T?) {
//        print("~\(senderType(sender))")
    }
}

class NSLogDebugOutputService : DebugOutputProtocol {
    func debugOutput<T>(_ message: String, sender: T?) {
        NSLog("\(senderType(sender)) \(message)")
    }
    func debugOutput(_ message: String) {
        NSLog(message)
    }
    
    func debugDeinit<T>(_ message: String, sender: T?) {
        NSLog("~\(senderType(sender)) \(message)")
    }
    func debugDeinit<T>(_ sender: T?) {
        NSLog("~\(senderType(sender))")
    }
}

class NoDebugOutputService : DebugOutputProtocol {
    func debugOutput<T>(_ message: String, sender: T?) { }
    func debugOutput(_ message: String) { }
    
    func debugDeinit<T>(_ message: String, sender: T?) { }
    func debugDeinit<T>(_ sender: T?) { }
}
