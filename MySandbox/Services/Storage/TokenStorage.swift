protocol TokenStorageProtocol {
    var token: String? { get set }
}

class MemoryTokenStorage: TokenStorageProtocol {
    var token: String?
}

class UserDefaultsTokenStorage: TokenStorageProtocol {
    let userDefaultsTokenKey: String
    let debugService: DebugOutputProtocol
    
    init(defaultsKey: String, debugService: DebugOutputProtocol) {
        self.userDefaultsTokenKey = defaultsKey
        self.debugService = debugService
    }
    
    var token: String? {
        get {
            return UserDefaults.standard.object(forKey: userDefaultsTokenKey) as? String
        }
        set {
            debugService.debugOutput("New token: \(newValue ?? "nil")", sender: self)
            UserDefaults.standard.set(newValue, forKey: userDefaultsTokenKey)
            UserDefaults.standard.synchronize()
        }
    }
}
