import Alamofire

class UnauthorizedHandlerPlugin: Plugin {
    let session = UserSession.shared
    
    func willProcessResponse<Model, ErrorModel>(response: (URLRequest?, HTTPURLResponse?, Data?, Error?), forRequest request: Request, formedFrom tronRequest: BaseRequest<Model, ErrorModel>) {
        if response.1?.statusCode == 401 {
            session.logout()
        }
    }
}
