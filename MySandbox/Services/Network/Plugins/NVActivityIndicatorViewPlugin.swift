import NVActivityIndicatorView

extension NVActivityIndicatorView: ActivityIndicatorProtocol {}

class NVActivityIndicatorViewPlugin: Plugin {
    var activityIndicatorView: NVActivityIndicatorView
    
    init(type: NVActivityIndicatorType? = .ballPulse, color: UIColor? = UIColor.MySandbox.someRGBColor) {
        activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 30, height: 60),
                                                        type: type,
                                                        color: color)
        
        if let topViewController = UIApplication.topViewController() {
            activityIndicatorView.center = topViewController.view.center
            topViewController.view.addSubview(activityIndicatorView)
            activityIndicatorView.bringSubview(toFront: topViewController.view)
        }
    }
    
    func willSendRequest(request: NSURLRequest?) {
        activityIndicatorView.startAnimating()
    }
    
    func requestDidReceiveResponse(response: (NSURLRequest?, HTTPURLResponse?, NSData?, NSError?)) {
        activityIndicatorView.stopAnimating()
    }
}

extension APIRequest {
    func progress(type: NVActivityIndicatorType? = .ballPulse, color: UIColor? = UIColor.MySandbox.someHEXColor) -> Self {
        let activityIndicatorViewPlugin = NVActivityIndicatorViewPlugin(type: type, color: color)
        plugins.append(activityIndicatorViewPlugin)
        return self
    }
}

extension UploadAPIRequest {
    func progress(type: NVActivityIndicatorType? = .ballPulse, color: UIColor? = UIColor.MySandbox.someHEXColor) -> Self {
        let activityIndicatorViewPlugin = NVActivityIndicatorViewPlugin(type: type, color: color)
        plugins.append(activityIndicatorViewPlugin)
        return self
    }
}
