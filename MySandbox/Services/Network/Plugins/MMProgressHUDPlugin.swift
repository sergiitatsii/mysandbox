import MMProgressHUD

class MMProgressHUDPlugin: Plugin {
    var progressTitle: String?
    var successTitle: String
    var errorTitle: String?
    
    init(progress: String, success: String, errorTitle: String?) {
        self.progressTitle = progress
        self.successTitle = success
        self.errorTitle = errorTitle
    }
    
    func willSendRequest(request: NSURLRequest?) {
        MMProgressHUD.show(withTitle: progressTitle)
    }
    
    func requestDidReceiveResponse(response: (NSURLRequest?, HTTPURLResponse?, NSData?, NSError?)) {
        if let error = response.3 {
            MMProgressHUD.dismissWithError(errorTitle ?? String(error.code))
        } else {
            MMProgressHUD.dismiss(withSuccess: successTitle)
        }
    }
}

extension APIRequest {
    func progressTitle(title: String, successTitle: String = "Success", errorTitle: String? = nil) -> Self {
        let progressPlugin = MMProgressHUDPlugin(progress: title,
                                                 success: successTitle, errorTitle: errorTitle)
        plugins.append(progressPlugin)
        return self
    }
}

extension UploadAPIRequest {
    func progressTitle(title: String, successTitle: String = "Success", errorTitle: String? = nil) -> Self {
        let progressPlugin = MMProgressHUDPlugin(progress: title,
                                                 success: successTitle, errorTitle: errorTitle)
        plugins.append(progressPlugin)
        return self
    }
}
