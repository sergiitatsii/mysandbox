class HeaderBuilder: HeaderBuildable {
    private let tokenStorage: TokenStorageProtocol = UserDefaultsTokenStorage(defaultsKey: Constants.UserDefaultsKeys.AuthToken, debugService: PrintDebugOutputService())
    
    func headers(forAuthorizationRequirement requirement: AuthorizationRequirement, including headers: [String : String]) -> [String : String] {
        var headers = headers
        headers["Accept"] = "application/json"
        
        switch requirement {
        case .none: return headers
        case .allowed :
            if let token = tokenStorage.token {
                headers["Authorization"] = "Token token=" + token
            }
            return headers
        case .required:
            let token = tokenStorage.token
            precondition(token != nil, "Auth token cannot be nil with AuthorizationRequirement.required")
            headers["Authorization"] = "Token token=" + token!
            return headers
        }
    }
}
