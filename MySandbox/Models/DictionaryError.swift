struct DictionaryError: JSONDecodable {
    let errors: [String:[String]]
    
    init(json: JSON) throws {
        if let errors = json["errors"].dictionaryObject as? [String:[String]] {
            self.errors = errors
        } else if let error = json.dictionary?["error"] {
            self.errors = ["error": [error.stringValue]]
        } else {
            throw DictionaryErrorSerializationError.missing("errors")
        }
    }
    
    init(errors: [String: [String]]) {
        self.errors = errors
    }
}

enum DictionaryErrorSerializationError: Error {
    case missing(String)
    case invalid(String, Any)
}

extension DictionaryError: Error {
    var localizedDescription: String {
        if let key = errors.keys.first, let value = errors[key]?.first {
            return "\(key): \(value)"
        }
        return "\(self)"
    }
}
