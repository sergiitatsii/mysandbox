class UserDraft {
    var firstName: String?
    var lastName: String?
    var email: String?
    var password: String?
    var confirmation: String?
    
    init(firstName: String?, lastName: String?, email: String?, password: String?, confirmation: String?) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.password = password
        self.confirmation = confirmation
    }
    
    init(){}
    
    func parametersForRequest() -> [String: Any] {
        var parameters = [String: Any]()
        parameters.addOptional(firstName, to: "user[first_name]")
        parameters.addOptional(lastName, to: "user[last_name]")
        parameters.addOptional(email, to: "user[email]")
        parameters.addOptional(password, to: "user[password]")
        parameters.addOptional(confirmation, to: "user[password_confirmation]")
        return parameters
    }
}
