struct AppToken: JSONDecodable {
    var token: String
    
    init?(token: String?) {
        guard let token = token else { return nil }
        self.token = token
    }
    
    init(json: JSON) throws {
        token = json["auth_token"].stringValue
    }
}

struct FacebookToken {
    var token: String
}

struct GoogleToken {
    var token: String
}
