protocol GoogleAuthorizationProtocol {
    func authorize()
    func signOut()
}

protocol GoogleAuthorizationDelegate: class {
    func googleAuthorizerSucceededWithToken(_ token: String)
    func googleAuthorizerFailedWithError(_ error: Error?)
}

protocol ActivityIndicatorProtocol {
    func startAnimating()
    func stopAnimating()
}

class GoogleAuthorizer: NSObject {
    weak var delegate: GoogleAuthorizationDelegate?
    var activityIndicator: ActivityIndicatorProtocol?
    
    init(delegate: GoogleAuthorizationDelegate, activityIndicator: ActivityIndicatorProtocol? = nil) {
        super.init()
        self.delegate = delegate
        self.activityIndicator = activityIndicator
        
        GIDSignIn.sharedInstance().clientID = "904177932559-cjhf2rr56iqr2bafcu6sj4r107j6sk94.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    private func continueSignIn(with token: String) {
        let googleToken = GoogleToken(token: token)
        
        let request = API.Session.open(googleToken).progress()
        request.stubbingEnabled = true
        request.apiStub.buildModel(fromFileNamed: "openSessionSuccess")
        request.apiStub.stubDelay = 0.5
        
        request.perform(withSuccess: { [weak self] session in
            self?.delegate?.googleAuthorizerSucceededWithToken(session.token)
            }, failure: { [weak self] error in
                self?.delegate?.googleAuthorizerFailedWithError(error.error)
        })
    }
}

// MARK: GIDSignInDelegate
extension GoogleAuthorizer: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            delegate?.googleAuthorizerFailedWithError(error)
        } else {
            self.continueSignIn(with: user.authentication.idToken)
        }
    }
}

// MARK: GIDSignInUIDelegate
extension GoogleAuthorizer: GIDSignInUIDelegate {
    private func signInWillDispatch(signIn: GIDSignIn!, error: Error!) {
        activityIndicator?.stopAnimating()
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        AppRouter.topViewController?.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        AppRouter.topViewController?.dismiss(animated: true, completion: nil)
    }
}

// MARK: GoogleAuthorizationProtocol
extension GoogleAuthorizer: GoogleAuthorizationProtocol {
    func authorize() {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func signOut() {
        GIDSignIn.sharedInstance().signOut()
    }
}
