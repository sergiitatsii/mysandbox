protocol FacebookAuthorizationProtocol {
    func authorize()
    func logOut()
}

protocol FacebookAuthorizationDelegate: class {
    func facebookAuthorizerSucceededWithToken(_ token: String)
    func facebookAuthorizerFailedWithError(_ error: Error?)
}

class FacebookAuthorizer: NSObject {
    weak var delegate: FacebookAuthorizationDelegate?
    
    init(button: FBSDKLoginButton?, delegate: FacebookAuthorizationDelegate) {
        super.init()
        self.delegate = delegate
        
        // when we use FBSDKLoginButton (not a custom button)
        guard let facebookButton = button else { return }
        facebookButton.readPermissions = AppEnvironment.shared.facebookPermissions
        facebookButton.delegate = self
    }
    
    private func continueLogIn(with token: String) {
        let facebookToken = FacebookToken(token: token)
        
        let request = API.Session.open(facebookToken).progress()
        request.stubbingEnabled = true
        request.apiStub.buildModel(fromFileNamed: "openSessionSuccess")
        //request.apiStub.successful = false
        //request.apiStub.buildError(fromFileNamed: "openSessionFailure")
        request.apiStub.stubDelay = 0.5
        
        request.perform(withSuccess: { [weak self] session in
            self?.delegate?.facebookAuthorizerSucceededWithToken(session.token)
            }, failure: { [weak self] error in
                if let _ = error.errorModel?.errors["phone"] {
                    if let emailErrors = error.errorModel?.errors["email"] {
                        if  emailErrors.contains("is invalid") {
                            self?.logOut()
                            AlertView.show(title: "ERROR".localized, message: "You Facebook account doesn't give us permission to process your email")
                        }
                    }
                } else {
                    self?.delegate?.facebookAuthorizerFailedWithError(error.error)
                    print("Open session for Facebook failed: \(String(describing: error.error?.localizedDescription))")
                }
        })
    }
}

// MARK: FBSDKLoginButtonDelegate
extension FacebookAuthorizer: FBSDKLoginButtonDelegate {
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if result.isCancelled {
            delegate?.facebookAuthorizerFailedWithError(nil)
            return
        }
        if error == nil {
            continueLogIn(with: result.token.tokenString)
        } else {
            delegate?.facebookAuthorizerFailedWithError(error)
        }
    }
}

// MARK: FacebookAuthorizationProtocol
extension FacebookAuthorizer: FacebookAuthorizationProtocol {
    func authorize() {
        FBSDKLoginManager().logIn(withReadPermissions: AppEnvironment.shared.facebookPermissions, from: nil, handler: { [weak self] result, error in
            if let result = result {
                if result.isCancelled {
                    self?.delegate?.facebookAuthorizerFailedWithError(nil)
                } else {
                    self?.continueLogIn(with: result.token.tokenString)
                }
            } else {
                self?.delegate?.facebookAuthorizerFailedWithError(error)
            }
        })
    }
    
    func logOut() {
        FBSDKLoginManager().logOut()
    }
}
